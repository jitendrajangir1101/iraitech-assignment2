from django.shortcuts import render ,redirect
from .forms import *
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect

# Create your views here.
def signup(request):
    
    form = signupForm(request.POST)
    if form.is_valid():
        form.save()
        messages.success(request ,'congratulations ! you are now registered with us')
        return HttpResponseRedirect('/login/')

    return render(request, 'signup.html' ,{'form' : form})
def login_user(request):
    if request.method == 'POST':
        form = loginForm(request=request, data=request.POST)
        if form.is_valid():
            uname = form.cleaned_data['username']
            upass = form.cleaned_data['password']
            user = authenticate(username=uname, password=upass)
            if user is not None:
                
                login(request, user)
                messages.success(request, 'logged in ')

                return HttpResponseRedirect('/index/')
                
    else:
        form = loginForm()
    return render(request, 'login.html', {'form': form})
def logout_user(request):
    logout(request)
    return redirect('signup')
def index(request):
    return render(request , 'index.html')