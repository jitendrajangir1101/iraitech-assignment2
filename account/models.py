from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser ,UserManager ,BaseUserManager


    
class member(AbstractUser): 
    email = models.EmailField( unique = True) 
    
    phone = models.CharField(max_length=10 ,unique=True)
    
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone'] 
    def __str__(self): 
       return "{}".format(self.email)

# Create your models here.
