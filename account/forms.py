from django import forms
from .models import member
from django.contrib.auth.forms import UserCreationForm ,AuthenticationForm , UsernameField
from django.contrib.auth.models import User
from django.utils.translation import gettext , gettext_lazy as _

class signupForm(UserCreationForm):
    class Meta:
        model = member
        fields = ('email','phone')
        widgets = {
        'email' :forms.EmailInput(attrs={'class':'form-control'}),
        'phone' :forms.TextInput(attrs={'class':'form-control'}),
        
        
        }

class loginForm(AuthenticationForm):
    username=UsernameField(label='Email' ,widget=forms.TextInput(attrs={'autofocus':True , 'class':'form-control'}))
    password=forms.CharField(label=_("Password"), strip=False , widget=forms.PasswordInput(attrs={'autocomplete':'current-password' , 'class':'form-control'}))

